<?php
/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 01.09.2019
 */

namespace Kudis;

use Bitrix\Main;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Sale\Internals\DiscountTable;

class BasketRule
{
    const DISCOUNT_TYPE_COMPARATOR = [
        'value' =>  'CurEach',
        'proc' =>  'Perc'
    ];

    private $sAction = 'add';

    private $iExistingRuleId;

    private $sXmlId = '';
    private $sSiteId = 's1';
    private $sCurrency = 'RUB';
    private $bActive = true;
    private $iSort = 100;
    private $iProirity = 1;
    private $sLastDiscount = 'Y';
    private $sLastLevelDiscount = 'N';
    private $arrUserGroups = [2];

    private $arDiscountElement;
    private $arTargets;

    private $LAST_ERROR;

    /**
     * BasketRule constructor.
     *
     * @return bool
     */
    public function __construct()
    {
        if (!self::checkModules()) {
            $this->ThrowException('Load modules error', 'MODULES_LOAD_ERROR');
            return false;
        }

        return true;

    }

    /**
     * Создаёт или обновляет правило для корзины или возвращает false
     * В этом случае нужно вызвать GetException() для получения сообщения об ошибке
     *
     * @param array $arDiscountElement
     * @return int|bool
     */
    public function loadRule(Array $arDiscountElement)
    {
        $this->arDiscountElement = $arDiscountElement;
        $this->checkExistingRule();

        if ($this->findAllTargetsIds()) {
            $arFields = [
                'XML_ID' => $this->sXmlId,
                'LID' => $this->sSiteId,
                'NAME' => $this->getName(),
                'CURRENCY' => $this->sCurrency,
                'ACTIVE_FROM' => $this->getActiveFrom(),
                'ACTIVE_TO' => $this->getActiveTo(),
                'ACTIVE' => $this->getActivity(),
                'SORT' => $this->iSort,
                'PRIORITY' => $this->iProirity,
                'LAST_DISCOUNT' => $this->sLastDiscount,
                'LAST_LEVEL_DISCOUNT' => $this->sLastLevelDiscount,
                'USER_GROUPS' => $this->arrUserGroups,
                'CONDITIONS' => [],
                'ACTIONS' => []
            ];

            if (!$this->checkTargetFormat()) {
                return false;
            }

            if (!$arFields['ACTIONS'] = $this->getActions()) {
                return false;
            }

            if (!$arFields['CONDITIONS'] = $this->getConditions()) {
                return false;
            }

            if ($this->sAction === 'update') {
                return \CSaleDiscount::Update($this->iExistingRuleId, $arFields);
            } else {
                return \CSaleDiscount::Add($arFields);
            }

        } else {
            return false;
        }
    }

    /**
     * Возвращает название правила из данных API
     *
     * @return string
     */
    private function getName()
    {
        if (isset($this->arDiscountElement['Name']) && !empty(trim($this->arDiscountElement['Name']))) {
            return trim($this->arDiscountElement['Name']);
        } else {
            return 'Rule ' . time();
        }
    }

    /**
     * Возвращает дату начала действия правила
     *
     * @return string
     */
    private function getActiveFrom()
    {
        if (
            isset($this->arDiscountElement['Period']['Start']) &&
            !empty(trim($this->arDiscountElement['Period']['Start'])) &&
            $sDateTime = ConvertTimeStamp(strtotime($this->arDiscountElement['Period']['Start']), 'FULL')
        ) {
            return $sDateTime;
        }

        return '';

    }

    /**
     * Возвращает дату окончания действия правила
     *
     * @return string
     */
    private function getActiveTo()
    {
        if (
            isset($this->arDiscountElement['Period']['End']) &&
            !empty(trim($this->arDiscountElement['Period']['End'])) &&
            $sDateTime = ConvertTimeStamp(strtotime($this->arDiscountElement['Period']['End']), 'FULL')
        ) {
            return $sDateTime;
        }

        return '';

    }

    /**
     * Возвращает активность правила
     *
     * @return string
     */
    private function getActivity()
    {
        if (array_key_exists('Active', $this->arDiscountElement)) {
            return $this->arDiscountElement['Active'] ? 'Y' : 'N';
        } else {
            return $this->bActive ? 'Y' : 'N';
        }
    }

    /**
     * Сопоставляет Guid из всех Target с элементами каталога
     * возвращает false, если хотя бы один элемент не был найден
     *
     * @return bool
     */
    private function findAllTargetsIds()
    {
        $arGuids = [];
        if (isset($this->arDiscountElement['Target']) && is_array($this->arDiscountElement['Target'])) {
            $arGuids = self::findGuidRecursively($this->arDiscountElement['Target']);
        }

        if (!empty($arGuids)) {

            $obProducts = \CIBlockElement::GetList(
                [],
                [
                    'ACTIVE' => 'Y',
                    '=XML_ID' => array_keys($arGuids)
                ],
                false,
                false,
                [
                    'ID',
                    'XML_ID'
                ]
            );

            while ($arProduct = $obProducts->Fetch()) {
                if (isset($arGuids[$arProduct['XML_ID']])) {
                    if (isset($arGuids[$arProduct['XML_ID']]['ElementId'])) {
                        $this->ThrowException(
                            'Two or many products with Guid: ' . $arProduct['XML_ID'],
                            'DOUBLE_PRODUCTS'
                        );
                        return false;
                    } else {
                        $arGuids[$arProduct['XML_ID']]['ElementId'] = $arProduct['ID'];
                    }
                }
            }

        } else {
            $this->ThrowException(
                'Can\'t find elements with Guids: ' . implode(', ', array_keys($arGuids)),
                'ALL_UNKNOWN_TARGETS'
            );
            return false;
        }

        $arMissedTargetGuids = [];
        foreach ($arGuids as $arTarget) {
            if (!isset($arTarget['ElementId']) && !in_array($arTarget['Guid'], $arMissedTargetGuids)) {
                $arMissedTargetGuids[] = $arTarget['Guid'];
            }
        }

        if (empty($arMissedTargetGuids)) {
            $this->arTargets = $arGuids;
            return true;
        } else {
            $this->ThrowException(
                'Can\'t find elements with Guids: ' . implode(', ', $arMissedTargetGuids),
                'SOME_UNKNOWN_TARGETS'
            );
            return false;
        }
    }

    /**
     * Выполняет поиск и возвращает все GUID целевых товаров
     *
     * @param $arData
     * @param array $arGuids
     * @return array
     */
    private static function findGuidRecursively($arData, $arGuids = [])
    {
        if (isset($arData) && is_array($arData)) {
            foreach ($arData as $arElement) {
                if (array_key_exists('Guid', $arElement)) {
                    if (!empty(trim($arElement['Guid'])) && !in_array($arElement['Guid'], array_keys($arGuids))) {
                        $arGuids[trim($arElement['Guid'])] = $arElement;
                    }
                } else {
                    if (is_array($arElement)) {
                        $arGuids = self::findGuidRecursively($arElement, $arGuids);
                    }
                }
            }
        }

        return $arGuids;

    }

    /**
     * Проверяет соответствие текущему формату API
     *
     * @return bool
     */
    private function checkTargetFormat()
    {
        $bHasError = false;

        if (
            isset($this->arDiscountElement['Target']) &&
            is_array($this->arDiscountElement['Target']) &&
            !self::isArrayAssoc($this->arDiscountElement['Target']) &&
            !empty($this->arDiscountElement['Target'])
        ) {

            foreach ($this->arDiscountElement['Target'] as $arTargetGroup) {
                $hasDiscountable = false;
                foreach ($arTargetGroup as $arTarget) {
                    if (
                        array_key_exists('Guid', $arTarget) &&
                        array_key_exists('Quantity', $arTarget)
                    ) {
                        if (array_key_exists('NoSetDiscount', $arTarget) && !$arTarget['NoSetDiscount']) {
                            $hasDiscountable = true;
                        }

                    } else {
                        $bHasError = true;
                        $this->ThrowException('Wrong target format', 'WRONG_API_TARGET_FORMAT');
                        break;
                    }
                }

                if ($bHasError) {
                    break;
                }

                if (!$hasDiscountable) {
                    $bHasError = true;
                    $this->ThrowException('Can\'t use this targets discount', 'WRONG_API_TARGET_VALUES');
                    break;
                }
            }
        }

        return !$bHasError;

    }

    /**
     * Поверяет, я вляется ли массив ассоциированым
     *
     * @param $arr
     * @return bool
     */
    public static function isArrayAssoc($arr)
    {
        $i = 0;
        foreach($arr as $k=>$val)
        {
            if("".$k!="".$i)
                return true;
            $i++;
        }
        return false;
    }

    /**
     * Формирует основыне правила скидки
     *
     * @return array|bool
     */
    private function getActions()
    {
        $arActions =  [
            'CLASS_ID' => 'CondGroup',
            'DATA' => [
                'All' => count($this->arDiscountElement['Target']) === 1 ? 'AND' : 'OR'
            ],
            'CHILDREN' => []
        ];

        if (!$arDiscount = $this->fillInDiscount()) {
            return false;
        }

        $arDiscountChild = [
            'CLASS_ID' => 'ActSaleBsktGrp',
            'DATA' => $arDiscount,
            'CHILDREN' => []
        ];

        foreach ($this->arDiscountElement['Target'] as $iKey => $arTargetGroup) {
            $arActions['CHILDREN'][$iKey] = $arDiscountChild;
            $arElements = [];
            foreach ($arTargetGroup as $arTarget) {
                if (!$arTarget['NoSetDiscount']) {
                    $arElements[] = $this->arTargets[$arTarget['Guid']]['ElementId'];
                }
            }
            $arActions['CHILDREN'][$iKey]['CHILDREN'][] = [
                'CLASS_ID' => 'CondIBElement',
                'DATA' => [
                    'logic' => 'Equal',
                    'value' => $arElements
                ]
            ];
        }

        return $arActions;

    }

    /**
     * Возвращает настройку скидки для правила
     *
     * @return array|bool
     */
    private function fillInDiscount()
    {
        $arDiscount = [
            'Type' => '',
            'Value' => 0,
            'Unit' => '',
            'Max' => 0,
            'All' => 'AND',
            'True' => 'True'
        ];
        if (
            isset($this->arDiscountElement['Discount']['Value']) &&
            $this->arDiscountElement['Discount']['Value'] > 0 &&
            isset($this->arDiscountElement['Discount']['Type']) &&
            isset(self::DISCOUNT_TYPE_COMPARATOR[$this->arDiscountElement['Discount']['Type']])
        ) {
            $arDiscount['Type'] = 'Discount';
            $arDiscount['Value'] = $this->arDiscountElement['Discount']['Value'];
            $arDiscount['Unit'] = self::DISCOUNT_TYPE_COMPARATOR[$this->arDiscountElement['Discount']['Type']];
            return $arDiscount;
        }

        $this->ThrowException('Wrong discount parameters', 'WRONG_DISCOUNT');
        return false;

    }

    /**
     * Формирует дополнительные правила применения скидки
     *
     * @return array|bool
     */
    private function getConditions()
    {
        if (count($this->arDiscountElement['Target']) === 1) {
            return $this->getConditionElement($this->arDiscountElement['Target'][0]);
        } else {
            $arConditions =  [
                'CLASS_ID' => 'CondGroup',
                'DATA' => [
                    'All' => count($this->arDiscountElement['Target']) === 1 ? 'AND' : 'OR',
                    'True' => 'True'
                ],
                'CHILDREN' => []
            ];

            foreach ($this->arDiscountElement['Target'] as $arTargetElement) {
                $arConditions['CHILDREN'][] = $this->getConditionElement($arTargetElement);
            }

            return $arConditions;
        }
    }

    /**
     * Преобразовывает группу элементов в правило
     *
     * @param array $arTargetGroup
     * @return array
     */
    private function getConditionElement($arTargetGroup = [])
    {
        $arConditionElement = [
            'CLASS_ID' => 'CondGroup',
            'DATA' => [
                'All' => 'AND',
                'True' => 'True'
            ],
            'CHILDREN' => []
        ];

        $arTargetGroupChild =  [
            'CLASS_ID' => 'CondBsktProductGroup',
            'DATA' => [
                'Found' => 'Found',
                'All' => 'AND'
            ],
            'CHILDREN' => [
                [
                    'CLASS_ID' => 'CondIBElement',
                    'DATA' => [
                        'logic' => 'Equal',
                        'value' => []
                    ]
                ],
                [
                    'CLASS_ID' => 'CondBsktFldQuantity',
                    'DATA' => [
                        'logic' => 'Equal',
                        'value' => 1
                    ]
                ]
            ]
        ];

        foreach ($arTargetGroup as $iKey => $arTarget) {
            $arConditionElement['CHILDREN'][$iKey] = $arTargetGroupChild;
            $arConditionElement['CHILDREN'][$iKey]['CHILDREN'][0]['DATA']['value'] = [$this->arTargets[$arTarget['Guid']]['ElementId']];
            if (isset($arTarget['Quantity']) && $arTarget['Quantity'] > 0) {
                $arConditionElement['CHILDREN'][$iKey]['CHILDREN'][1]['DATA']['value'] = $arTarget['Quantity'];
            }
        }

        return $arConditionElement;

    }

    /**
     * Устанавливает ошибку
     *
     * @param string $sMessage
     * @param bool $iCode
     *
     * @void
     */
    private function ThrowException($sMessage, $iCode = false)
    {
        if (
            is_object($sMessage) &&
            (
                is_subclass_of($sMessage, 'CApplicationException') ||
                (strtolower(get_class($sMessage))=='capplicationexception')
            )
        ) {
            $this->LAST_ERROR = $sMessage;
        } else {
            $this->LAST_ERROR = new \CApplicationException($sMessage, $iCode);
        }
    }

    /**
     * Возвращает последнюю ошибку
     *
     * @return \CApplicationException
     */
    public function GetException()
    {
        return $this->LAST_ERROR;
    }

    /**
     * Проверяет возможность подключения и подгружает модули
     *
     * @return bool
     */
    private static function checkModules()
    {
        try {
            if (
                !Loader::includeModule('catalog') ||
                !Loader::includeModule('iblock') ||
                !Loader::includeModule('sale')
            ) {
                return false;
            }
        } catch (LoaderException $e) {
            return false;
        }

        return true;

    }

    /**
     * Проверяет наличие правила с таким же Guid и устанавливает значения из настроек существующего правила
     *
     * @void
     */
    private function checkExistingRule()
    {
        if (isset($this->arDiscountElement['Guid']) && !empty($this->arDiscountElement['Guid'])) {

            $this->sXmlId = $this->arDiscountElement['Guid'];

            $obDiscount = DiscountTable::getList([
                'filter' => ['XML_ID' => $this->sXmlId],
                'select' => [
                    'ID',
                    'LID',
                    'ACTIVE',
                    'SORT',
                    'PRIORITY',
                    'LAST_DISCOUNT',
                    'LAST_LEVEL_DISCOUNT'
                ]
            ]);

            if ($arDiscount = $obDiscount->fetch()) {

                $this->iExistingRuleId = $arDiscount['ID'];
                $this->sAction = 'update';

                $this->sSiteId = $arDiscount['LID'];
                $this->bActive = $arDiscount['ACTIVE'] === 'Y';

                $this->iSort = (int) $arDiscount['SORT'];
                $this->iProirity = (int) $arDiscount['PRIORITY'];
                $this->sLastDiscount = $arDiscount['LAST_DISCOUNT'];
                $this->sLastLevelDiscount = $arDiscount['LAST_LEVEL_DISCOUNT'];

            }
        }
    }
}
