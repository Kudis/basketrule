# Тестовое задание

По некому API приходит описание скидки:
    
```bash
Array
    (
        [Guid] => b577427d-c3fa-11e9-80c6-00155da7d607
        [Name] => Набор аксессуаров DJI Osmo Pocket Expansion Kit (Part 13) и Экшн-камера DJI Osmo Pocket
        [Active] => 1
        [Period] => Array
            (
                [Start] => 
                [End] => 
            )

        [Рartners] => Array
            (
            )

        [Promocodes] => Array
            (
            )

        [Target] => Array
            (
                [0] => Array
                    (
                        [0] => Array
                            (
                                [Guid] => 9f8ff181-f353-11e8-80be-00155da7d607
                                [Name] => Экшн-камера DJI Osmo Pocket
                                [Quantity] => 1
                                [NoSetDiscount] => 1
                            )

                        [1] => Array
                            (
                                [Guid] => 7cf77d2c-f4a9-11e8-80be-00155da7d607
                                [Name] => Набор аксессуаров DJI Osmo Pocket Expansion Kit (Part 13)
                                [Quantity] => 1
                                [NoSetDiscount] => 
                            )

                    )

            )

        [Gift] => Array
            (
            )

        [Discount] => Array
            (
                [Type] => value
                [Value] => 2400
            )

    )

)

```
Guid - внешний для битрикса идентификатор, чаще всего соответствует полю XML_ID.

Target - массив товаров участвующих в скидке.

NoSetDiscount - флаг: 1 - скидка не вычитается из цены товара, 0 скидка вычитается из цены товара.

Quantity - количество товара необходимое для выполнения условия скидки.

Discount - массив описание скидки.

Type: тип значения value - значение в рублях (может быть proc - значение в процентах)

Value: сумма в рублях (или в процентах соответственно)

Текущая скидка читается так:
для каждых Экшн-камера DJI Osmo Pocket 1шт. и Набор аксессуаров DJI Osmo Pocket Expansion Kit (Part 13) 1шт., предоставляется скидка 2400 руб., которая вычитается из цены  DJI Osmo Pocket Expansion Kit (Part 13).

Задача: написать функцию, которая трансформирует описание скидки, полученное по API в правило корзины битрикса.

При проверке, внимание будет обращаться на ход мыслей и стиль кода.

Способ использования
----------------------------

Запускаем:

```bash
use Kudis\BasketRule;

$arDiscountElement = [
    'Guid' => 'b577427d-c3fa-11e9-80c6-00155da7d607',
    'Name' => 'Набор аксессуаров DJI Osmo Pocket Expansion Kit (Part 13) и Экшн-камера DJI Osmo Pocket',
    'Active' => true,
    'Period' => [
        'Start' => '',
        'End' => ''
    ],
    'Рartners' => [],
    'Promocodes' => [],
    'Target' => [
        [
            [
                'Guid' => '9f8ff181-f353-11e8-80be-00155da7d607',
                'Name' => 'Экшн-камера DJI Osmo Pocket',
                'Quantity' => 1,
                'NoSetDiscount' => true
            ],
            [
                'Guid' => '7cf77d2c-f4a9-11e8-80be-00155da7d607',
                'Name' => 'Набор аксессуаров DJI Osmo Pocket Expansion Kit (Part 13)',
                'Quantity' => 1,
                'NoSetDiscount' => false
            ]
        ]
    ],
    'Gift' => [],
    'Discount' => [
        'Type' => 'value',
        'Value' => 2400
    ]
];

$obRule = new BasketRule();
if ($obRule) {
    if ($iRuleId = $obRule->loadRule($arDiscountElement)) {
        var_dump($iRuleId);
    } else {
        var_dump($obRule->GetException());
    }
} else {
    var_dump($obRule->GetException());
}
```